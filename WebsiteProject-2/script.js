function check(form){
	var account = {
		'1234' : {'pin': '1234'},
		'5678' : {'pin': '5678'},
		'0000' : {'pin': '0000'}
	}

	var accountNo = form.inputUser.value;
    var pin = form.inputPassword.value;
	
	if(accountNo != null && account[accountNo]['pin'] == pin){
		return true;
	}else{
		alert("เลขประจำตัวประชาชนหรือรหัสประจำตัวไม่ถูกต้อง !");
		return false;
	}
}